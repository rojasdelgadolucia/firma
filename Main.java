package com.example.programacionsegura;
public class Main {
    public static void main(String[] args) {
        try {
            GeneradorClaves.main(args);
            CifradoYFirma.main(args);
            DescifrarYFirmar.main(args);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}