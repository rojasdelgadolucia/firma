package com.example.programacionsegura;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.security.PublicKey;
import java.security.Signature;
import javax.crypto.Cipher;

public class DescifrarYFirmar {
    public static void main(String[] args) throws Exception {
        // Leer el mensaje cifrado desde el archivo "mensajeCifrado"
        FileInputStream mensajeCifradoIS = new FileInputStream("mensajeCifrado"); // Creamos un flujo de entrada para leer el mensaje cifrado desde un archivo
        byte[] mensajeCifrado = mensajeCifradoIS.readAllBytes(); // Leemos todos los bytes del mensaje cifrado
        mensajeCifradoIS.close(); // Cerramos el flujo de entrada

        // Leer la clave pública desde el archivo "publicKey"
        ObjectInputStream publicKeyIS = new ObjectInputStream(new FileInputStream("publicKey")); // Creamos un lector de objetos para leer la clave pública desde un archivo
        PublicKey publicKey = (PublicKey) publicKeyIS.readObject(); // Convertimos el objeto leído en una clave pública
        publicKeyIS.close(); // Cerramos el lector de objetos

        // Descifrar el mensaje utilizando el algoritmo RSA y la clave pública
        Cipher cipher = Cipher.getInstance("RSA"); // Creamos un objeto Cipher para realizar operaciones de descifrado utilizando el algoritmo RSA
        cipher.init(Cipher.DECRYPT_MODE, publicKey); // Inicializamos el descifrado con la clave pública en modo descifrado
        byte[] mensajeDescifrado = cipher.doFinal(mensajeCifrado); // Desciframos el mensaje cifrado y obtenemos un arreglo de bytes como resultado

        // Leer la firma del mensaje desde el archivo "firmaMensaje"
        FileInputStream firmaIS = new FileInputStream("firmaMensaje"); // Creamos un flujo de entrada para leer la firma desde un archivo
        byte[] firma = firmaIS.readAllBytes(); // Leemos todos los bytes de la firma
        firmaIS.close(); // Cerramos el flujo de entrada

        // Verificar la firma utilizando la clave pública
        Signature signature = Signature.getInstance("SHA256withRSA"); // Creamos un objeto Signature para realizar operaciones de verificación utilizando SHA-256 con RSA
        signature.initVerify(publicKey); // Inicializamos la verificación con la clave pública
        signature.update(mensajeDescifrado); // Actualizamos la verificación con el mensaje descifrado
        boolean verificado = signature.verify(firma); // Verificamos la firma con el mensaje descifrado y obtenemos un resultado booleano

        // Mostrar el resultado de la verificación de la firma
        if (verificado) {
            System.out.println("Mensaje descifrado y firma verificada correctamente."); // Si la firma es válida, mostramos un mensaje de éxito
            System.out.println("Mensaje original: " + new String(mensajeDescifrado)); // Mostramos el mensaje original descifrado
        } else {
            System.out.println("La firma no corresponde al mensaje original."); // Si la firma no es válida, mostramos un mensaje de error
        }
    }
}