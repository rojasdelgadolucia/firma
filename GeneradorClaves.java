package com.example.programacionsegura;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.security.*;
public class GeneradorClaves {
    public static void main(String[] args) throws Exception {
        // Generar un par de claves RSA
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA"); //generador de pares de claves RSA
        keyPairGenerator.initialize(2048); // Inicializamos el generador para que genere claves de 2048 bits
        KeyPair keyPair = keyPairGenerator.generateKeyPair(); // Generamos un par de claves pública y privada

        // Guardar la clave pública en un archivo
        ObjectOutputStream publicKeyOS = new ObjectOutputStream(new FileOutputStream("publicKey")); // Creamos un flujo de salida de objetos para escribir en un archivo llamado "publicKey"
        publicKeyOS.writeObject(keyPair.getPublic()); // Escribimos la clave pública en el archivo
        publicKeyOS.close(); // Cerramos el flujo de salida

        // Guardar la clave privada en un archivo
        ObjectOutputStream privateKeyOS = new ObjectOutputStream(new FileOutputStream("privateKey")); // Creamos un flujo de salida de objetos para escribir en un archivo llamado "privateKey"
        privateKeyOS.writeObject(keyPair.getPrivate()); // Escribimos la clave privada en el archivo
        privateKeyOS.close(); // Cerramos el flujo de salida
    }
}
