package com.example.programacionsegura;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
public class Hash {
    public static void main(String[] args) {

        String algoritmo = "SHA-256";
        try {
            MessageDigest md = MessageDigest.getInstance(algoritmo);
            byte[] c1 = "Primera cadena".getBytes();
            md.update(c1);
            byte[] resumen = md.digest();
            System.out.println("Resumen: " + new String(resumen));
        }catch (NoSuchAlgorithmException e){
            throw  new RuntimeException(e);
        }
    }
}

