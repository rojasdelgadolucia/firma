package com.example.programacionsegura;
import java.io.*;
import java.security.PrivateKey;
import java.security.Signature;
import javax.crypto.Cipher;

public class CifradoYFirma {
    public static void main(String[] args) throws Exception {
        // Solicitar al usuario que ingrese el mensaje a cifrar
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); // Creamos un lector para leer desde la entrada estándar (consola)
        System.out.println("Por favor, ingrese el mensaje que desea cifrar:"); // Solicitamos al usuario que ingrese el mensaje
        String mensaje = br.readLine(); // Leemos el mensaje ingresado por el usuario desde la consola

        // Leer la clave privada desde el archivo "privateKey"
        ObjectInputStream privateKeyIS = new ObjectInputStream(new FileInputStream("privateKey")); // Creamos un lector de objetos para leer la clave privada desde un archivo
        PrivateKey privateKey = (PrivateKey) privateKeyIS.readObject(); // Convertimos el objeto leído en una clave privada
        privateKeyIS.close(); // Cerramos el lector de objetos

        // Cifrar el mensaje utilizando el algoritmo RSA y la clave privada
        Cipher cipher = Cipher.getInstance("RSA"); // Creamos un objeto Cipher para realizar operaciones de cifrado utilizando el algoritmo RSA
        cipher.init(Cipher.ENCRYPT_MODE, privateKey); // Inicializamos el cifrado con la clave privada en modo cifrado
        byte[] mensajeCifrado = cipher.doFinal(mensaje.getBytes()); // Ciframos el mensaje y obtenemos un arreglo de bytes como resultado

        // Firmar el mensaje utilizando el algoritmo de firma SHA-256 con RSA
        Signature signature = Signature.getInstance("SHA256withRSA"); // Creamos un objeto Signature para realizar operaciones de firma utilizando SHA-256 con RSA
        signature.initSign(privateKey); // Inicializamos la firma con la clave privada
        signature.update(mensaje.getBytes()); // Actualizamos la firma con el mensaje original
        byte[] firma = signature.sign(); // Firmamos el mensaje y obtenemos un arreglo de bytes como resultado

        // Guardar el mensaje cifrado en un archivo llamado "mensajeCifrado"
        FileOutputStream mensajeCifradoOS = new FileOutputStream("mensajeCifrado"); // Creamos un flujo de salida para escribir el mensaje cifrado en un archivo
        mensajeCifradoOS.write(mensajeCifrado); // Escribimos el mensaje cifrado en el archivo
        mensajeCifradoOS.close(); // Cerramos el flujo de salida

        // Guardar la firma en un archivo llamado "firmaMensaje"
        FileOutputStream firmaOS = new FileOutputStream("firmaMensaje"); // Creamos un flujo de salida para escribir la firma en un archivo
        firmaOS.write(firma); // Escribimos la firma en el archivo
        firmaOS.close(); // Cerramos el flujo de salida

        // Confirmar que el mensaje ha sido cifrado y firmado correctamente
        System.out.println("El mensaje ha sido cifrado y firmado correctamente."); // Mostramos un mensaje de confirmación
    }
}