package com.example.programacionsegura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgramacionSeguraApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProgramacionSeguraApplication.class, args);
    }

}
